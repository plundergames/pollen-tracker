import {runWith} from "firebase-functions";
import {firestore} from "firebase-admin";

export const exportPollenData = runWith({
  invoker: "public",
}).region("europe-west1").https
    .onRequest((req, res) => {
      return loadPollenData("Baden-Württemberg",
          "Oberrhein und unteres Neckartal")
          .then((data) => res.send(data) as any);
    });

/**
 * Loads the data from DB
 */
async function loadPollenData(region: string,
    partRegion: string) {
  const db = firestore();
  return db.collection("pollenData")
      .orderBy("date", "asc")
      .get()
      .then((querySnapshot) => {
        return querySnapshot.docs.map((doc) => ({
          date: doc.id,
          ...doc.data().pollenValues[region][partRegion],
        }));
      });
}
