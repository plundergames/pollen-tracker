import {runWith} from "firebase-functions";
import {firestore} from "firebase-admin";
import {applyCORS} from "../utils/requests";

// use this for the rates:
// https://www.bundesfinanzministerium.de/Web/DE/Themen/Steuern/Steuerarten/Umsatzsteuer/Umsatzsteuer_Umrechnungskurse/umsatzsteuer_umrechnungskurse.html

export const exportExchangeData = runWith({
  invoker: "public",
}).region("europe-west1").https
    .onRequest(applyCORS((req, res) => {
      return loadExchangeData("usa")
          .then((data) => res.send(data) as any);
    }));

/**
 * Loads the data from DB
 */
async function loadExchangeData(country: string) {
  const db = firestore();
  return db.collection("exchangeData")
      .get()
      .then((querySnapshot) => {
        return querySnapshot.docs.map((doc) => ({
          date: doc.id,
          value: doc.data()[country],
        }));
      });
}
