import {runWith} from "firebase-functions";
import {applyCORS} from "../utils/requests";
import {firestore} from "firebase-admin";

export const exportMeals = runWith({
  invoker: "public",
}).region("europe-west1")
    .https
    .onRequest(applyCORS(async (req, res) => {
      return loadMeals()
          .then((data) => data.map((doc) => {
            const {createdAt, categories, ...data} = doc.data();
            return ({
              id: doc.id,
              date: createdAt.toDate().toISOString(),
              categories: Object.fromEntries(categories.map((category: string) => [category, 1])),
              ...data,
            });
          }))
          .then((data) => res.send(data) as any);
    })
    );

async function loadMeals(snapshot?: firestore.QueryDocumentSnapshot): Promise<firestore.QueryDocumentSnapshot[]> {
  const db = firestore();
  const baseQuery = db.collection("meals")
      .limit(1000)
      .orderBy("createdAt", "asc");
  const snapshots = await (snapshot ? baseQuery.startAfter(snapshot) : baseQuery).get();

  return [
    ...snapshots.docs,
    ...snapshots.size === 1000 ?
      await loadMeals(snapshots.docs[snapshots.size - 1]) :
      [],
  ];
}
