import {runWith} from "firebase-functions";
import {applyCORS} from "../utils/requests";
import {updateHolidays} from "../utils/updateHolidays";

/**
 * https://www.schulferien.org/deutschland/ferien/2023/
 * http://127.0.0.1:5001/pollen-tracking/europe-west1/queryHolidays/deutschland/2023
 * https://europe-west1-pollen-tracking.cloudfunctions.net/queryHolidays/deutschland/2023
 */
export const queryHolidays = runWith({
  invoker: "public",
}).region("europe-west1")
    .https
    .onRequest(applyCORS(async (req, res) => {
      const [
        ,
        country,
        year,
      ] = req.path.split("/");

      if (!country || !year) {
        return res.status(400)
            .send(`Received: ${[country, year]}. Use like this: /deutschland/2020`);
      }

      const table = await updateHolidays(country, Number(year));
      return res.send({table});
    })
    );
