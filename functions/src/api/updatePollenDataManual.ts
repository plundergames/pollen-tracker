import {runWith} from "firebase-functions";
import {updatePollenData} from "../utils/updatePollenData";

export const updatePollenDataManual = runWith({
  timeoutSeconds: 60,
  invoker: "public",
}).region("europe-west1").https
    .onRequest((_, res) => updatePollenData()
        .then((data) => res.send(data) as any));
