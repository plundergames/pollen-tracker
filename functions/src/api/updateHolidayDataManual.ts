import {runWith} from "firebase-functions";
import {updateHolidayData} from "../utils/updateHolidayData";

/**
 * http://127.0.0.1:5001/pollen-tracking/europe-west1/updateHolidayDataManual
 * https://europe-west1-pollen-tracking.cloudfunctions.net/updateHolidayDataManual
 */
export const updateHolidayDataManual = runWith({
  timeoutSeconds: 60 * 9,
  invoker: "public",
}).region("europe-west1").https
    .onRequest((_, res) => updateHolidayData()
        .then((data) => res.send(data) as any));
