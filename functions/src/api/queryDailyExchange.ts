import {runWith} from "firebase-functions";
import {applyCORS} from "../utils/requests";

/**
 * http://127.0.0.1:5001/pollen-tracking/europe-west1/queryDailyExchange/USD/EUR/2023-04-01/2023-06-30
 * https://europe-west1-pollen-tracking.cloudfunctions.net//queryDailyExchange/USD/EUR/2023-04-01/2023-06-30
 */
export const queryDailyExchange = runWith({
  invoker: "public",
}).region("europe-west1").https
    .onRequest(applyCORS(async (req, res) => {
      const [
        ,
        fromCurrency,
        toCurrency,
        startISODate,
        endISODate,
      ] = req.path.split("/");

      if (!fromCurrency || !toCurrency || !startISODate || !endISODate) {
        return res.status(400)
            .send(`Received: ${[
              fromCurrency,
              toCurrency,
              startISODate,
              endISODate,
            ]}. Use like this: /USD/EUR/2023-01-01/2023-03-31`);
      }

      const url = getHistoricalUrl(
          fromCurrency,
          toCurrency,
          startISODate,
          endISODate);
      const data = await fetch(url).then((response) => response.json());
      return res.send(data);
    }));

function getHistoricalUrl(
    fromCurrency: string,
    toCurrency: string,
    startISODate: string,
    endISODate: string): string {
  const timestampStart = Math.floor(new Date(startISODate).getTime() / 1000);
  const timestampEnd = Math.floor(new Date(endISODate).getTime() / 1000);
  return `https://query2.finance.yahoo.com/v8/finance/chart/${fromCurrency}${toCurrency}=X?formatted=true&crumb=wRB.DunOu79&lang=en-US&region=US&includeAdjustedClose=true&interval=1d&period1=${timestampStart}&period2=${timestampEnd}&events=capitalGain|div|split&useYfid=true&corsDomain=finance.yahoo.com`;
}
