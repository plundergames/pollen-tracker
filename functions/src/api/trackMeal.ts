import {runWith} from "firebase-functions";
import {applyCORS} from "../utils/requests";
import {defineSecret} from "firebase-functions/params";
import OpenAI from "openai";
import {zodResponseFormat} from "openai/helpers/zod";
import {WhatsappMedia, WhatsAppMessagePayload, WhatsAppWebhookPayload} from "../types/whatsapp.types";
import {MealInformationSchema} from "../types/mealInformation.types";
import {readFile} from "fs/promises";
import {firestore} from "firebase-admin";
import {addDays} from "date-fns";
import {ingredientsOptions, mealCategoryOptions} from "../types/mealOptions";

const whatsappToken = defineSecret("WHATSAPP_TOKEN");
const whatsappId = defineSecret("WHATSAPP_ID");
const whatsappVerifyToken = defineSecret("WHATSAPP_VERIFY_TOKEN");
const openaiApiKey = defineSecret("OPENAI_API_KEY");

export const trackMeal = runWith({
  invoker: "public",
  secrets: ["WHATSAPP_TOKEN", "WHATSAPP_ID", "WHATSAPP_VERIFY_TOKEN", "OPENAI_API_KEY"],
}).region("europe-west1")
    .https
    .onRequest(applyCORS(async ({query, method, body}, res) => {
      if (method === "GET") {
        const mode = query["hub.mode"];
        const token = query["hub.verify_token"];
        const challenge = query["hub.challenge"];

        if (mode === "subscribe" && token === whatsappVerifyToken.value()) {
          console.info("Webhook verified");
          res.status(200).send(challenge);
        } else {
          res.sendStatus(403);
        }
        return;
      }

      const webhookPayload = body as WhatsAppWebhookPayload;
      const change = webhookPayload.entry[0].changes[0].value;
      const message = change.messages?.[0];

      if (!message || !message.image) {
        return res.sendStatus(200);
      }

      console.info("message", message);

      const timestamp = (Number(message.timestamp) * 1000) || Date.now();
      if (timestamp < addDays(new Date(), -1).getTime()) {
        await sendResponse(message.from, `Old message from ${new Date(timestamp).toISOString()}, will not be handled!`);
        return res.sendStatus(200);
      }

      const imageUrl = await fetchFacebookImage(message.image.id);
      const photoResponse = await analyzePhoto(imageUrl);
      await saveMealInformation(photoResponse);
      const responseBody = photoResponse.parsed ? JSON.stringify(photoResponse.parsed, null, 2) : (photoResponse.refusal ?? "Unknown error!");
      await sendResponse(message.from, responseBody);

      return res.sendStatus(200);
    })
    );

async function fetchFacebookImage(photoId: string): Promise<string> {
  const imageUrlResponse = await fetch(
      `https://graph.facebook.com/v21.0/${photoId}`,
      {
        method: "GET",
        headers: {
          "Authorization": `Bearer ${whatsappToken.value()}`,
          "Content-Type": "application/json",
        },
      }
  );
  const {url, mime_type: mimeType}: WhatsappMedia = await imageUrlResponse.json();
  const imageResponse = await fetch(url, {
    method: "GET",
    headers: {
      "Authorization": `Bearer ${whatsappToken.value()}`,
    },
  });
  const imageBuffer = process.env.FUNCTIONS_EMULATOR === "true" ? await readFile("src/test-data/breakfast.jpg") : Buffer.from(await imageResponse.arrayBuffer());
  const base64Image = imageBuffer.toString("base64");
  return `data:${mimeType};base64,${base64Image}`;
}

async function analyzePhoto(imageUrl: string) {
  const openai = new OpenAI({
    apiKey: openaiApiKey.value(),
  });

  const completion = await openai.beta.chat.completions.parse({
    model: "gpt-4o",
    max_completion_tokens: 16384,
    messages: [
      {
        role: "user",
        content: [
          {
            type: "text",
            text: ["Analysiere die Mahlzeit und extrahiere die Zutaten sowie die Nährwerte. Schätze in Gramm. Beschränke die Beschreibung auf 1-2 Sätze.",
              `Suche die Zutaten aus folgender List: ${ingredientsOptions.join(",")}`,
              `Suche die Kategorien aus folgdener List: ${mealCategoryOptions.join(",")}`,
            ].join("\n"),
          },
          {
            type: "image_url",
            image_url: {
              url: imageUrl,
            },
          },
        ],
      },
    ],
    response_format: zodResponseFormat(MealInformationSchema, "mealInformation"),
  });

  return completion.choices[0].message;
}

async function saveMealInformation(mealInformation: Awaited<ReturnType<typeof analyzePhoto>>) {
  const db = firestore();
  if (mealInformation.parsed) {
    const {ingredients, ...data} = mealInformation.parsed;
    await db.collection("meals").add({
      ...data,
      ingredients: ingredients.reduce((acc, {name, grams}) => {
        acc[name] = grams;
        return acc;
      }, {} as Record<string, number>),
      createdAt: new Date(),
    });
  }
}

async function sendResponse(toNumber: string, body: string) {
  await fetch(
      `https://graph.facebook.com/v21.0/${whatsappId.value()}/messages`,
      {
        method: "POST",
        headers: {
          "Authorization": `Bearer ${whatsappToken.value()}`,
          "Content-Type": "application/json",
        },
        body: JSON.stringify(
        {
          messaging_product: "whatsapp",
          recipient_type: "individual",
          to: toNumber,
          type: "text",
          text: {
            body,
          },
        } satisfies WhatsAppMessagePayload
        ),
      }
  );
}
