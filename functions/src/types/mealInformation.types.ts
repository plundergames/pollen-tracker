import {z} from "zod";

export const MealInformationSchema = z.object({
  ingredients: z.object({
    name: z.string(),
    grams: z.number(),
  }).array(),
  description: z.string(),
  categories: z.string().array(),
  calories: z.number(),
  protein: z.number(),
  fat: z.number(),
  carbohydrates: z.number(),
  fiber: z.number(),
  sugar: z.number(),
  salt: z.number(),
  glycemicLoad: z.number(),
});

export type MealInformation = z.infer<typeof MealInformationSchema>;
