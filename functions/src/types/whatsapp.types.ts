export type WhatsAppWebhookPayload = {
  object: "whatsapp_business_account";
  entry: {
    id: string;
    changes: {
      value: {
        messaging_product: "whatsapp";
        metadata: {
          display_phone_number: string;
          phone_number_id: string;
        };
        contacts: {
          profile: {
            name: string;
          };
          wa_id: string;
        }[];
        messages: {
          from: string;
          id: string;
          timestamp: string;
          text?: {
            body: string;
          };
          image?: WhatsappImage;
          type: "text" | "image";
        }[];
      };
      field: "messages";
    }[];
  }[];
};

export interface WhatsappImage {
  id: string;
  link: string;
  mime_type: string;
  caption?: string;
}

export type WhatsAppMessagePayload = {
  messaging_product: "whatsapp";
  recipient_type: "individual";
  to: string;
  type: "text";
  text: {
    body: string;
  };
  context?: {
    message_id: string;
  };
};

export interface WhatsappMedia {
  messaging_product: "whatsapp",
  url: string,
  mime_type: string,
  sha256: string,
  file_size: string,
  id: string
}
