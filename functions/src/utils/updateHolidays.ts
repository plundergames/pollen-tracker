import * as cheerio from "cheerio";
import {eachDayOfInterval, endOfYear, formatISO, getMonth, isBefore, parse, setYear, startOfYear} from "date-fns";
import {pipeWith} from "pipe-ts";
import {firestore} from "firebase-admin";


export async function updateHolidays(country: string, year: number) {
  const url = `https://www.schulferien.org/${country}/ferien/${year}/`;
  const html = await fetch(url).then((response) => response.text());
  const data = await parseHtml(html);
  const table = await parseTable(data, Number(year));
  await setHolidays(country, year, table);
  return table;
}

async function setHolidays(country: string,
    year: number,
    data: {date: string, data: Record<string, number>}[]) {
  const db = firestore();

  const parentDocRef = db.collection("holidayData").doc(String(year));
  await parentDocRef.set({year}, {merge: true});

  await parentDocRef
      .collection("holidayCountries")
      .doc(country)
      .set({data, country, year});
}

async function parseHtml(data: string) {
  const $ = cheerio.load(data);

  return $(".contentbox > table > tbody").children().toArray()
      .map((row) => {
        return $(row)
            .find("td > * > span:not([class*=\"fussnote\"])")
            .map((_, elem) =>
              $(elem)
                  .contents()
                  .filter((i, el) => el.nodeType === 3)
                  .text()
                  .replaceAll(/\s\s+|\n|\t/g, "").trim())
            .toArray();
      });
}

async function parseTable(data: string[][], year: number) {
  const table = createEmptyTable(year);
  return [...data.reduce((acc, row) => {
    const [state, ...rowData] = row;

    if (!state) {
      return acc;
    }

    const escapedState = state
        .replaceAll("é", "e")
        .replaceAll("ç", "c");

    const dates = getAffectedDays(rowData, year);
    dates.forEach((date) => {
      const dateData = acc.get(date);
      if (dateData) {
        dateData[escapedState] = 1;
      }
    });
    return acc;
  }, table)]
      .filter(([, data]) => Object.keys(data).length > 0)
      .map(([date, data]) => ({date, data}));
}

function createEmptyTable(year: number) {
  const start = startOfYear(new Date(year, 0, 1));
  const end = endOfYear(new Date(year, 0, 1));

  const allDatesOfYear = eachDayOfInterval({start, end});
  return new Map(allDatesOfYear.map((date) => ([formatISO(date), {} as Record<string, number>])));
}

function getAffectedDays(intervals: string[], year: number) {
  return intervals.flatMap((interval) => interval
      .split("+")
      .map((s) => s.trim())
      .flatMap((s) => {
        const [start, end] = s.split("-").map((s) => s.trim());

        if (!end) {
          return [parseDate(start, year)];
        }

        const [startDate, endDate] = pipeWith([parseDate(start, year), parseDate(end, year)],
            ([startDate, endDate]) => isBefore(endDate, startDate) ?
          getMonth(endDate) === 0 ?
            [setYear(startDate, year - 1), endDate] :
            [startDate, setYear(endDate, year + 1)] as const :
          [startDate, endDate] as const);

        return eachDayOfInterval({start: startDate, end: endDate});
      })
  ).map((date) => formatISO(date));
}

function parseDate(date: string, year: number) {
  return parse(date, "dd.MM.", new Date(year, 0, 1));
}
