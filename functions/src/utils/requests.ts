import {Response, Request} from "express";
import * as cors from "cors";

export function sendJsonResponse(
    res: Response,
    responseBody: any,
    {statusCode = 200}: Partial<{ statusCode: number }> = {}
) {
  res.status(statusCode);
  res.send(responseBody);
}

export function keepConnectionAlive(res: Response) {
  res.setHeader("Keep-Alive", "timeout=540");
  res.setHeader("Content-Type", "application/json");

  const timer = setInterval(() => res.write(" "), 5000);
  return (result: any) => {
    clearInterval(timer);
    res.write(JSON.stringify(result));
    res.end();
  };
}

export function sendErrorResponse(
    res: Response,
    errorName: string,
    error: { message: string; code: string }
) {
  console.info(errorName, error);

  sendJsonResponse(
      res,
      {
        status: "error",
        code: error.code,
        message: error.message,
      },
      {statusCode: 400}
  );
}

const corsHandler = cors({origin: true});
export const applyCORS =
  (
      handler: (
      req: Request,
      res: Response
    ) => Promise<unknown>
  ) =>
    (req: Request, res: Response) => {
      return corsHandler(req, res, (_) => {
        return handler(req, res);
      });
    };
