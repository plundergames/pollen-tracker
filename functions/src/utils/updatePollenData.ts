import {firestore} from "firebase-admin";
import {addDays, format, startOfDay} from "date-fns";
// eslint-disable-next-line @typescript-eslint/no-var-requires
import {rollup} from "d3-array";
import {deepMapToObject} from "./utils";

/**
 * Fetches pollen data from DWD and stores it in the database.
 */
export async function updatePollenData() {
  const dataRecords = await getPollenData();
  await storePollenData(dataRecords);
  return deepMapToObject(dataRecords);
}

/**
 * Fetches pollen data from DWD and processes it
 */
async function getPollenData() {
  const data = await fetch("https://opendata.dwd.de/climate_environment/health/alerts/s31fg.json").then((res) => res.json());
  const flattened = data.content.flatMap((area: any) => {
    return Object.entries(area.Pollen).flatMap(([pollenType, values]) => {
      return Object.entries(values as any).map(([relativeDay, pollenValue]) => {
        return {
          ...area,
          relativeDay,
          pollenType,
          pollenValue,
        };
      });
    });
  });
  return rollup(flattened as any[],
      (result) => rollup(result,
          ([v]) => getPollenValue(v.pollenValue),
          (v) => v.pollenType),
      (v) => v.relativeDay,
      (v) => v.region_name,
      (v) => v.partregion_name || "All",
  );
}

/**
 * Stores pollen data in the database.
 * @param data the pollen data to store
 */
async function storePollenData(data: Map<string, any>) {
  const db = firestore();
  const collection = db.collection("pollenData");
  const batch = db.batch();
  [...data].forEach(([relativeDay, record]) => {
    const date = getDay(relativeDay);
    const id = format(date, "yyyy-MM-dd");
    const doc = collection.doc(id);
    const pollenValues = deepMapToObject(record);
    console.log(pollenValues);
    batch.set(doc, {
      date,
      pollenValues,
    });
  });
  await batch.commit();
}

/**
 * Map the day to a date.
 * @param relativeDay
 */
function getDay(relativeDay: string) {
  const today = startOfDay(new Date());
  switch (relativeDay) {
    case "today":
      return today;
    case "tomorrow":
      return addDays(today, 1);
    default:
      return addDays(today, 2);
  }
}

/**
 * Map the pollen value to a number.
 */
function getPollenValue(stringValue: string) {
  switch (stringValue) {
    case "0":
    default:
      return 0;
    case "0-1":
      return 0.5;
    case "1":
      return 1;
    case "1-2":
      return 1.5;
    case "2":
      return 2;
    case "2-3":
      return 2.5;
    case "3":
      return 3;
  }
}
