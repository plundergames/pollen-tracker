/**
 * transform a deep map to an object
 */
export function deepMapToObject(map: any) {
  if (map instanceof Map) {
    return [...map].reduce((acc, [key, value]) => {
      acc[key] = deepMapToObject(value);
      return acc;
    }, {} as Record<string, any>);
  } else {
    return map;
  }
}
