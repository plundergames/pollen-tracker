import {eachYearOfInterval, getYear} from "date-fns";
import {firestore} from "firebase-admin";
import {updateHolidays} from "./updateHolidays";

export async function updateHolidayData() {
  const years = eachYearOfInterval({start: new Date("2018-01-01"), end: new Date()});
  const countries = ["deutschland", "frankreich", "schweiz"];

  const db = firestore();
  const availableYears = await db.collection("holidayData").get()
      .then((snapshot) => snapshot.docs.map((doc) => Number(doc.id)));
  console.info(`Available years: ${availableYears}`);

  for (const year of years.filter((year) => !availableYears.includes(getYear(year)))) {
    for (const country of countries) {
      console.info(`Updating holidays for ${country} in ${getYear(year)}`);
      try {
        await updateHolidays(country, getYear(year));
      } catch (e) {
        console.error(`Could not update holidays for ${country} in ${getYear(year)}`, e);
      }
    }
  }

  return queryAllHolidayData();
}

function queryAllHolidayData() {
  const db = firestore();

  return db.collectionGroup("holidayCountries").get()
      .then((snapshot) => snapshot.docs.map((doc) => doc.data()));
}
