import {initializeApp} from "firebase-admin/app";

export {updatePollenDataAutomatic} from "./schedule/updatePollenDataAutomatic";
export {updatePollenDataManual} from "./api/updatePollenDataManual";
export {exportPollenData} from "./api/exportPollenData";
export {exportExchangeData} from "./api/exportExchangeData";
export {queryDailyExchange} from "./api/queryDailyExchange";
export {queryHolidays} from "./api/queryHolidays";
export {updateHolidayDataManual} from "./api/updateHolidayDataManual";
export {updateHolidayDataAutomatic} from "./schedule/updateHolidayDataAutomatic";
export {trackMeal} from "./api/trackMeal";
export {exportMeals} from "./api/exportMeals";

initializeApp();
