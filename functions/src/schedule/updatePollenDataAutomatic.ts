import {pubsub} from "firebase-functions";
import "cross-fetch/polyfill";
import {updatePollenData} from "../utils/updatePollenData";

export const updatePollenDataAutomatic = pubsub
    .schedule("0 15 * * *")
    .timeZone("Europe/Berlin")
    .onRun(updatePollenData);


