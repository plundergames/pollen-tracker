import {pubsub} from "firebase-functions";
import {updateHolidayData} from "../utils/updateHolidayData";

export const updateHolidayDataAutomatic = pubsub
    .schedule("0 4 1 */3 *")
    .timeZone("Europe/Berlin")
    .onRun(updateHolidayData);
